import json
import os
import random

import lorem

import generate_skilldata

# ==================================================================================================

random_skills = 100

urls = ["https://github.com/Random/Skill", "https://gitlab.com/Random/Skill"]
allowed_colors = generate_skilldata.allowed_colors
icons_path = generate_skilldata.icons_path
skilldata_path = generate_skilldata.skilldata_path


# ==================================================================================================


def get_randomized_icon():
    icon_color = random.choice(allowed_colors)
    icon_name = random.choice(os.listdir(icons_path))
    return icon_name, icon_color


# ==================================================================================================


def generate_skill():
    skilldata = {
        "url": random.choice(urls),
        "published": random.randint(17000, 18000),
        "stars": random.randint(0, 100),
    }

    if "github" in skilldata["url"]:
        skilldata["gitlogo"] = "resources/github.png"
    elif "gitlab" in skilldata["url"]:
        skilldata["gitlogo"] = "resources/gitlab.png"
    else:
        skilldata["gitlogo"] = "resources/git.png"

    skilldata["languages"] = random.choice([["de", "en"], ["de"], ["en"]])
    skilldata["architectures"] = random.choice([["arm64", "amd64"], ["arm64"]])

    skilldata["name"] = {}
    skilldata["description_short"] = {}
    skilldata["description_long"] = {}

    extra_tag = "".join([t[0] for t in skilldata["languages"]]) + " "
    extra_tag += "".join([t[0] for t in skilldata["architectures"]]) + " "
    if "en" in skilldata["languages"]:
        name = " ".join(lorem.sentence().split()[0 : random.randint(1, 2)])
        skilldata["name"]["en"] = name
        skilldata["description_short"]["en"] = extra_tag + lorem.sentence()
        skilldata["description_long"]["en"] = extra_tag + lorem.paragraph()
    else:
        skilldata["name"]["en"] = ""
        skilldata["description_short"]["en"] = ""
        skilldata["description_long"]["en"] = ""
    if "de" in skilldata["languages"]:
        name = " ".join(lorem.sentence().split()[0 : random.randint(1, 2)])
        skilldata["name"]["de"] = name
        skilldata["description_short"]["de"] = extra_tag + lorem.sentence()
        skilldata["description_long"]["de"] = extra_tag + lorem.paragraph()
    else:
        skilldata["name"]["de"] = ""
        skilldata["description_short"]["de"] = ""
        skilldata["description_long"]["de"] = ""

    # Generate skill icon
    icon_name = generate_skilldata.make_icon(*get_randomized_icon())
    icon_name = "generated/icons/" + icon_name
    skilldata["symbol"] = icon_name

    skilldata["security"] = {
        "needs_internet_access": random.choice([True, False, False]),
        "non_standard_topics": random.choice([True, False, False]),
        "extra_container_flags": random.choice(["", "", "--device /dev/snd"]),
    }

    author = " ".join(lorem.sentence().split()[0 : random.randint(1, 2)])
    skilldata["author"] = author.upper()

    return skilldata


# ==================================================================================================


def main():
    skill_entries = []
    for _ in range(random_skills):
        skill_entries.append(generate_skill())

    with open(skilldata_path, "w+", encoding="utf-8") as file:
        json.dump(skill_entries, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
