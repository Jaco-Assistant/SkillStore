import datetime
import json
import os
import re

import cv2
import numpy as np
import requests
import tqdm
import yaml

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
generated_path = file_path + "../generated/"

skills_path = file_path + "../skills.json"
skilldata_path = generated_path + "skilldata.json"
icons_path = file_path + "../resources/icons/"
output_icons_path = generated_path + "icons/"

allowed_colors = ["#ff3333", "#ff8200", "#2adcce", "#5533ff", "#bf2680", "#b2b600"]
allowed_html_tags = ["<br>", "</br>", "<p>", "</p>", "<i>", "</i>", "<a>", "</a>"]
allowed_architectures = ["arm64", "amd64"]

storefile_keys = {
    "languages",
    "name",
    "description_short",
    "description_long",
    "symbol",
    "color",
    "author",
    "architectures",
}

config_keys = {
    "extra_container_flags",
    "has_action",
    "needs_internet_access",
    "topics_read",
    "topics_write",
}

min_text_length = {
    "name": 3,
    "author": 3,
    "description_short": 15,
    "description_long": 100,
}
max_text_length = {
    "name": 19,
    "author": 15,
    "description_short": 85,
    "description_long": 2000,
}

# Because the repository hosters have different user numbers the number of stars is not comparable
# The factor is based on a comparison between some repositories which are mirrored to both platforms
star_compare_factor = {
    "github": 15,
    "gitlab": 1,
}


# ==================================================================================================


def hex2bgr(h):
    rgb = tuple(int(h[i : i + 2], 16) for i in (0, 2, 4))
    bgr = [rgb[2], rgb[1], rgb[0]]
    return bgr


def date2days(datestr):
    d = datetime.datetime.strptime(datestr, "%d.%m.%Y") - datetime.datetime(1970, 1, 1)
    return d.days


# ==================================================================================================


def update_url(url, filename):
    """Update repository url for the given file depending on the specific hoster"""

    if "gitlab.com" in url:
        url = url + "/-/raw/master/" + filename
    elif "github.com" in url:
        url = url.replace("github.com", "raw.githubusercontent.com")
        url = url + "/master/" + filename
    else:
        return None

    return url


# ==================================================================================================


def download_jsonfile(url, filename):
    """Download and convert the store file for given skill repository url"""

    url = update_url(url, filename)
    r = requests.get(url)
    try:
        store_entry = json.loads(r.content)
    except json.decoder.JSONDecodeError:
        return None

    return store_entry


# ==================================================================================================


def download_yamlfile(url, filename):
    """Download and convert the store file for given skill repository url"""

    url = update_url(url, filename)
    r = requests.get(url)
    try:
        store_entry = yaml.safe_load(r.content)
    except yaml.parser.ParserError:
        return None

    return store_entry


# ==================================================================================================


def check_config_entry(entry):
    """Checks and cleans the config file of a skill."""

    # Checks store file only has needed keys
    if "system" not in entry and config_keys == (entry["system"].keys()):
        print("Config keys not matching")
        return None

    ha = entry["system"]["has_action"]
    if not (ha is False or ha is True):
        print("Action not valid")
        return None

    nia = entry["system"]["needs_internet_access"]
    if not (nia is False or nia is True):
        print("Internet not valid")
        return None

    if not isinstance(entry["system"]["topics_read"], list) or not isinstance(
        entry["system"]["topics_write"], list
    ):
        print("Topics not valid")
        return None

    non_std_topics = False
    tops = entry["system"]["topics_read"]
    tops.extend(entry["system"]["topics_write"])

    for t in tops:
        if t.startswith("Jaco/"):
            if (
                not t.startswith("Jaco/Skills/SayText")
                and t != "Jaco/Skills/UserAnswer"
                and t != "Jaco/Intents/GreedyText"
                and not t.startswith("Jaco/Intents/SkillDialogs")
            ):
                non_std_topics = True
    entry["non_standard_topics"] = non_std_topics

    return entry


# ==================================================================================================


def check_store_entry(entry):
    """Checks and cleans the store entry file of a skill."""

    # Checks store file only has needed keys
    if not storefile_keys == (entry.keys()):
        print("Storefile keys not matching")
        return None

    # Remove not allowed chars and check length of color, symbol and language
    entry["color"] = re.sub(r"[^0-9abcdef#]", "", entry["color"].lower())
    entry["symbol"] = re.sub(r"[^0-9a-z\-._]", "", entry["symbol"].lower())
    if len(entry["color"]) != 7:
        print("Color not valid:", entry["color"])
        return None
    for i, t in enumerate(entry["languages"]):
        t = t.lower()
        t = re.sub(r"[^a-z]", "", t)
        entry["languages"][i] = t
        if len(t) != 2:
            print("Language not valid:", t)
            return None
    for i, t in enumerate(entry["architectures"]):
        t = t.lower()
        t = re.sub(r"[^a-z0-9]", "", t)
        entry["architectures"][i] = t
        if t not in allowed_architectures:
            print("Architecture not valid:", t)
            return None

    # Check name and descriptions have all language keys
    if not set(entry["languages"]) == (entry["name"].keys()):
        print("Names not completely translated")
        return None
    if not set(entry["languages"]) == (entry["description_short"].keys()):
        print("Short descriptions not completely translated")
        return None
    if not set(entry["languages"]) == (entry["description_long"].keys()):
        print("Long descriptions not completely translated")
        return None

    # Check author, name and descriptions, remove not allowed chars and tags and check length
    t = entry["author"].upper()
    t = re.sub(r"[<>]", "", t)
    entry["author"] = t
    if len(t) < min_text_length["author"] or len(t) > max_text_length["author"]:
        print("Author not valid:", t)
        return None
    for k in entry["name"].copy():
        t = entry["name"][k].upper()
        t = re.sub(r"[<>]", "", t)
        entry["name"][k] = t
        if len(t) < min_text_length["name"] or len(t) > max_text_length["name"]:
            print("Name is too long or too short:", t)
            return None
    for k in entry["description_short"].copy():
        t = re.sub(r"[<>]", "", entry["description_short"][k])
        t = t.replace("'", "&#39;")
        entry["description_short"][k] = t
        if (
            len(t) < min_text_length["description_short"]
            or len(t) > max_text_length["description_short"]
        ):
            print("Short description is too long or too short:", t)
            return None
    for k in entry["description_long"].copy():
        t = entry["description_long"][k]
        t = t.replace("'", "&#39;")
        matches = re.findall(r"(<[a-zA-Z0-9]+>)", t)
        matches2 = re.findall(r"(<\/[a-zA-Z0-9]+>)", t)
        matches.extend(matches2)
        for tag in matches:
            if tag not in allowed_html_tags:
                t = t.replace(tag, "")
        entry["description_long"][k] = t
        if (
            len(t) < min_text_length["description_long"]
            or len(t) > max_text_length["description_long"]
        ):
            print("Long description is too long or too short:", t)
            return None

    return entry


# ==================================================================================================


def get_star_score(url):
    """Query and calculate the star score for given repository url"""

    if "github.com" in url:
        url = url.replace("github.com/", "api.github.com/repos/")
        try:
            r = requests.get(url)
            repo_info = json.loads(r.content)
            star_score = repo_info["stargazers_count"]
        except Exception:
            return 0

    elif "gitlab.com" in url:
        name = url.split("gitlab.com/")[1]
        url = url.replace(name, "")
        name = name.replace("/", "%2F")
        url = url.replace("gitlab.com/", "gitlab.com/api/v4/projects/")
        url = url + name
        try:
            r = requests.get(url)
            repo_info = json.loads(r.content)
            star_score = repo_info["star_count"]
        except Exception:
            return 0

    else:
        return 0

    # Apply comparison factor
    if "github" in url:
        star_score = int(star_score / star_compare_factor["github"] + 0.499)
    elif "gitlab" in url:
        star_score = int(star_score / star_compare_factor["gitlab"] + 0.499)

    return star_score


# ==================================================================================================


def make_icon(icon_name, icon_color):
    """Make new colored icon"""

    icon_path = icons_path + icon_name
    if not os.path.exists(icon_path) or icon_color not in allowed_colors:
        return None

    img = cv2.imread(icon_path, cv2.IMREAD_UNCHANGED)
    icon_color = icon_color.replace("#", "")
    color = hex2bgr(icon_color)

    # Replace areas of transparency with white and not transparent and remove alpha channel
    trans_mask = img[:, :, 3] == 0
    img[trans_mask] = [255, 255, 255, 255]
    img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)

    # Convert all non black pixels to white and all black pixels to the given color
    img[np.where((img != [0, 0, 0]).all(axis=2))] = [255, 255, 255]
    img[np.where((img == [0, 0, 0]).all(axis=2))] = color

    icon_name, _ = os.path.splitext(icon_name)
    icon_name = icon_name + "-" + icon_color + ".png"
    cv2.imwrite(output_icons_path + icon_name, img)

    return icon_name


# ==================================================================================================


def collect_skilldata(skill):
    """Download and check skill infos, generate skill icon"""

    skilldata = {
        "url": skill["url"],
        "published": date2days(skill["published"]),
    }

    # Download config infos from skill repository
    config_entry = download_yamlfile(skilldata["url"], "config.template.yaml")
    if config_entry is None:
        print(" This skill's config file is unreachable or broken:", skilldata["url"])
        return None
    config_entry = check_config_entry(config_entry)
    if config_entry is None:
        print(" The config file of this skill is invalid:", skilldata["url"])
        return None
    skilldata["security"] = {
        "needs_internet_access": config_entry["system"]["needs_internet_access"],
        "non_standard_topics": config_entry["non_standard_topics"],
        "extra_container_flags": config_entry["system"]["extra_container_flags"],
    }

    # Download store infos from skill repository
    store_entry = download_jsonfile(skilldata["url"], "skillstore.json")
    if store_entry is None:
        print(" This skill's store file is unreachable or broken:", skilldata["url"])
        return None
    store_entry = check_store_entry(store_entry)
    if store_entry is None:
        print(" The store file of this skill is invalid:", skilldata["url"])
        return None
    skilldata["languages"] = store_entry["languages"]
    skilldata["name"] = store_entry["name"]
    skilldata["author"] = store_entry["author"]
    skilldata["description_short"] = store_entry["description_short"]
    skilldata["description_long"] = store_entry["description_long"]
    skilldata["architectures"] = store_entry["architectures"]

    # Calculate star score
    star_score = get_star_score(skilldata["url"])
    if star_score is not None:
        skilldata["stars"] = star_score
    else:
        return None

    # Set repository link logo
    if "github" in skilldata["url"]:
        skilldata["gitlogo"] = "resources/github.png"
    elif "gitlab" in skilldata["url"]:
        skilldata["gitlogo"] = "resources/gitlab.png"
    else:
        skilldata["gitlogo"] = "resources/git.png"

    # Generate skill icon
    icon_name = make_icon(store_entry["symbol"], store_entry["color"])
    if icon_name is not None:
        icon_name = "generated/icons/" + icon_name
        skilldata["symbol"] = icon_name
    else:
        return None

    return skilldata


# ==================================================================================================


def main():
    with open(skills_path, "r", encoding="utf-8") as file:
        skilldata = json.load(file)

    if not os.path.isdir(output_icons_path):
        os.mkdir(output_icons_path)

    skill_entries = []
    for skill in tqdm.tqdm(skilldata):
        skilldata = collect_skilldata(skill)
        if skilldata is not None:
            skill_entries.append(skilldata)

    with open(skilldata_path, "w+", encoding="utf-8") as file:
        json.dump(skill_entries, file, indent=2)


# ==================================================================================================

if __name__ == "__main__":
    main()
