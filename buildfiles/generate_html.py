import json
import os

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
generated_path = skilldata_path = file_path + "../generated/"

skilldata_path = generated_path + "skilldata.json"
skill_template_path = file_path + "skilltab_template.html"
index_template_path = file_path + "index_template.html"

language = "en"
architecture = "arm64"

with open(skilldata_path, "r", encoding="utf-8") as file:
    skilldata = json.load(file)
with open(skill_template_path, "r", encoding="utf-8") as file:
    skill_template = file.read()
with open(index_template_path, "r", encoding="utf-8") as file:
    index_template = file.read()

# ==================================================================================================

skill_entries = []
for skill in skilldata:

    se = skill_template
    se = se.replace("{{SYMBOLPATH}}", skill["symbol"])

    hide_skill = False
    if architecture in skill["architectures"]:
        if language in skill["languages"]:
            tlang = language
        elif "xx" in skill["languages"]:
            tlang = "xx"
        else:
            hide_skill = True
    else:
        hide_skill = True

    if hide_skill:
        se = se.replace("{{HIDDENSTYLE}}", "d-none")
    else:
        se = se.replace("{{SKILLNAME}}", skill["name"][tlang])
        se = se.replace("{{SHORTTEXT}}", skill["description_short"][tlang])
        se = se.replace("{{AUTHOR}}", skill["author"])
        se = se.replace("{{HIDDENSTYLE}}", "")

    # Write all data for sorting, language changing and skill descriptions
    # into data tags for easy access
    data_tags = "data-stars='{}' data-published='{}' data-url='{}' data-languages='{}'"
    data_tags += " data-name='{}' data-author='{}' data-dshort='{}' data-dlong='{}'"
    data_tags += " data-gitlogo='{}' data-security='{}' data-architectures='{}'"
    data_tags = data_tags.format(
        skill["stars"],
        skill["published"],
        skill["url"],
        json.dumps(skill["languages"]),
        json.dumps(skill["name"]),
        skill["author"],
        json.dumps(skill["description_short"]),
        json.dumps(skill["description_long"]),
        skill["gitlogo"],
        json.dumps(skill["security"]),
        json.dumps(skill["architectures"]),
    )
    se = se.replace("{{SORTINGDATA}}", data_tags)

    skill_entries.append(se)

skill_entry_text = "\n" + "\n".join(skill_entries)
index_text = index_template.replace("{{SKILLTABS}}", skill_entry_text)

with open(generated_path + "../index.html", "w+", encoding="utf-8") as file:
    file.write(index_text)
