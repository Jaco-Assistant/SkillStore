FROM docker.io/ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y nano
RUN apt-get install -y python3-pip
RUN apt-get install -y git
RUN apt-get install -y curl

RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir numpy
RUN pip3 install --no-cache-dir opencv-python-headless
RUN pip3 install --no-cache-dir tqdm
RUN pip3 install --no-cache-dir requests
RUN pip3 install --no-cache-dir lorem
RUN pip3 install --no-cache-dir pyyaml

# This was only required for the first icon export
# RUN apt-get install -y python2
# RUN apt-get install -y python-pip
# RUN apt-get install -y inkscape
# RUN pip2 install --no-cache-dir lxml
# RUN pip2 install --no-cache-dir pyyaml
# RUN git clone --depth 1 https://gitlab.com/gmgeo/osmic.git

# Clear cache to save space, only has an effect if image is squashed
RUN apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /Skill-Store/
CMD ["/bin/bash"]
