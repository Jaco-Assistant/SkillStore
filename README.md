# Jaco Skill-Store

Website to display all skills for Jaco. Check it out [here](https://jaco-store.web.app/).

<div align="center">
    <img src="resources/jaco.png" alt="jaco logo" width="75%"/>
</div>

<br/>

[![pipeline status](https://gitlab.com/Jaco-Assistant/SkillStore/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/SkillStore/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/SkillStore/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/SkillStore/-/commits/master)

<br/>

## Setup

Build container:

```bash
docker build -t skill_store - < Containerfile
```

Connect to container:

```bash
docker run --network host --rm \
  --volume `pwd`/:/Skill-Store/ \
  -it skill_store
```

Generate the store page:

```bash
# Run in container

python3 buildfiles/generate_skilldata.py
python3 buildfiles/generate_html.py
```

## Debugging

Run the store locally by opening the `index.html` file in a browser.

Generate random skill entries for testing purposes:

```bash
# Run in container
python3 buildfiles/random_skilldata.py
```

Run tests: \
(Check out `gitlab-ci.yml` for the steps)

```bash
docker build --no-cache -t testing_jaco_sks - < tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/:/Skill-Store/ \
  -it testing_jaco_sks
```

## Hosting

Install firebase-cli:

```bash
sudo apt-get install -y npm
sudo npm install -g firebase-tools
```

Initialize firebase:

```bash
mkdir Firebase && cd Firebase
firebase login
firebase init
```

Copy websites to `Firebase/public/`.

Upload website with `firebase deploy`. \
If there are problems when uploading, update firebase and logout and login again.
