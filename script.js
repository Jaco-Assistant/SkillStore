var current_language = "en";
var current_architecture = "arm64";

var clickCount = 0;
var timeout = 300;

// Single and Double click on same element, See https://stackoverflow.com/a/43384481
function clicks(callback1, callback2, target) {
  clickCount++;
  if (clickCount == 1) {
    setTimeout(function() {
      if (clickCount == 1) {
        show_skill_infos(target.parentNode);
      } else {
        toggle_skill_selection(target);
      }
      clickCount = 0;
    }, timeout || 300);
  }
}

// Sort all skills by selected attribute
function sort_skills(attribute) {
  $(".skillentries")
    .find(".skillentry")
    .sort(function(a, b) {
      return $(b).attr(attribute) - $(a).attr(attribute);
    })
    .appendTo(".skillentries");
}

// Change language of all skills, hide those which don't have the selected language or architecture
function filter_skills() {
  document.querySelectorAll(".skillentry").forEach(function(skill) {
    let languages = skill.getAttribute("data-languages");
    let architectures = skill.getAttribute("data-architectures");
    let hide_skill = false;
    let tlang = "";

    if (architectures.includes(current_architecture)) {
      if (languages.includes(current_language)) {
        tlang = current_language;
      } else if (languages.includes("xx")) {
        tlang = "xx";
      } else {
        hide_skill = true;
      }
    } else {
      hide_skill = true;
    }

    if (hide_skill) {
      if (skill.children[0].classList.contains("skillselected")) {
        skill.children[0].classList.remove("skillselected");
      }
      skill.classList.add("d-none");
    } else {
      let name = skill.getAttribute("data-name");
      let dshort = skill.getAttribute("data-dshort");
      name = JSON.parse(name)[tlang];
      dshort = JSON.parse(dshort)[tlang];
      let desc = skill.getAttribute("data-author") + "<br>" + dshort;

      skill.children[1].innerHTML = name;
      skill.children[2].innerHTML = desc;
      if (skill.classList.contains("d-none")) {
        skill.classList.remove("d-none");
      }
    }
  });
}

// Call dropdown actions
document.querySelectorAll(".dropdown-item").forEach(el =>
  el.addEventListener("click", event => {
    let key = event.target.innerHTML;
    let topelem = event.target.parentNode.parentNode;
    let menu = topelem.children[0];
    if (topelem.id === "dropdown-sorting") {
      if (key === "Stars") {
        sort_skills("data-stars");
        menu.innerHTML = "Sorting: " + key;
      } else if (key === "Published") {
        sort_skills("data-published");
        menu.innerHTML = "Sorting: " + key;
      }
    } else if (topelem.id === "dropdown-architecture") {
      current_architecture = event.target.getAttribute("data-arch-key");
      filter_skills();
      menu.innerHTML = "Architecture: " + key;
    } else if (topelem.id === "dropdown-language") {
      current_language = event.target.getAttribute("data-lang-key");
      filter_skills();
      menu.innerHTML = "Language: " + key;
    }
  })
);

// Select skill on double click
function toggle_skill_selection(target) {
  if (target.classList.contains("skillselected")) {
    target.classList.remove("skillselected");
  } else {
    target.classList.add("skillselected");
  }
}

// Show skill infos on single click
document.querySelectorAll(".skillimg").forEach(el =>
  el.addEventListener("click", event => {
    clicks(show_skill_infos, toggle_skill_selection, event.target);
  })
);

function unselect_all_skills() {
  document.querySelectorAll(".skillselected").forEach(function(item) {
    item.classList.remove("skillselected");
  });
}

// Unselect all skills on button click
document.getElementById("button-unselect").addEventListener("click", event => {
  unselect_all_skills();
});

// Show import/export modal and prefill with selected skill urls
function show_import_export(key) {
  if (key === "import") {
    document.getElementById("import-export-import").style.visibility =
      "visible";
    document.getElementById("import-export-cancel").innerHTML = "Cancel";
  } else if (key === "export") {
    document.getElementById("import-export-import").style.visibility = "hidden";
    document.getElementById("import-export-cancel").innerHTML = "Close";
  }

  let urls = [];
  document.querySelectorAll(".skillselected").forEach(function(item) {
    urls.push(item.parentNode.getAttribute("data-url"));
  });

  let area = document.getElementById("import-export-area");
  area.value = "";
  urls.forEach(function(item) {
    item = item + "\n";
    area.value += item;
  });
  $("#import-export").modal("show");
}

// Add click listener to show import/export modal window
document.getElementById("button-import").addEventListener("click", event => {
  show_import_export("import");
});
document.getElementById("button-export").addEventListener("click", event => {
  show_import_export("export");
});

// Select all skills from the import list
$("#import-export-form").submit(function() {
  let urls = document.getElementById("import-export-area").value.split("\n");
  unselect_all_skills();
  document.querySelectorAll(".skillimg").forEach(function(item) {
    let skillurl = item.parentNode.getAttribute("data-url");
    if (urls.includes(skillurl)) {
      item.classList.add("skillselected");
    }
  });

  // Don't reload page
  $("#import-export").modal("hide");
  return false;
});

// Show skill modal and prefill with selected skill data
function show_skill_infos(skill) {
  let languages = skill.getAttribute("data-languages");
  let tlang = "";

  if (languages.includes(current_language)) {
    tlang = current_language;
  } else if (languages.includes("xx")) {
    tlang = "xx";
  } else {
    return;
  }

  let name = skill.getAttribute("data-name");
  name = JSON.parse(name)[tlang];
  document.getElementById("skill-infos-title").innerHTML = name;

  let dlong = skill.getAttribute("data-dlong");
  dlong = JSON.parse(dlong)[tlang];
  document.getElementById("skill-infos-text").innerHTML = dlong;

  // Show or hide security notes
  let sec = skill.getAttribute("data-security");
  sec = JSON.parse(sec);
  let show = false;
  let elem = document.getElementById("skill-infos-sec-intent");
  if (sec["non_standard_topics"]) {
    elem.style.display = "block";
    show = true;
  } else {
    elem.style.display = "none";
  }
  elem = document.getElementById("skill-infos-sec-wifi");
  if (sec["needs_internet_access"]) {
    elem.style.display = "block";
    show = true;
  } else {
    elem.style.display = "none";
  }
  elem = document.getElementById("skill-infos-sec-flags");
  if (sec["extra_container_flags"] !== "") {
    elem.style.display = "block";
    let flagtext = '"' + sec["extra_container_flags"] + '"';
    document.getElementById("sec-flags-content").innerHTML = flagtext;
    show = true;
  } else {
    elem.style.display = "none";
  }
  elem = document.getElementById("skill-infos-security");
  if (show) {
    elem.style.display = "block";
  } else {
    elem.style.display = "none";
  }

  let url = skill.getAttribute("data-url");
  let link = document.getElementById("skill-infos-link");
  link.href = url;
  let gitlogo = skill.getAttribute("data-gitlogo");
  link.children[0].src = gitlogo;

  let but_add = document.getElementById("skill-infos-add");
  if (skill.children[0].classList.contains("skillselected")) {
    but_add.innerHTML = "Remove";
  } else {
    but_add.innerHTML = "Add";
  }
  // Replace add button with clone to remove old event listeners
  elClone = but_add.cloneNode(true);
  but_add.parentNode.replaceChild(elClone, but_add);
  elClone.addEventListener("click", event => {
    toggle_skill_selection(skill.children[0]);
  });

  $("#skill-infos").modal("show");
}

// Sort skills on page load
sort_skills("data-stars");
